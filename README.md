# Riemann Sum Visualizer
Visually demonstrating how the volume under a surface can be approximated by breaking it down into a number of small components, and how the number of divisions can improve this approximation.
[View Source Code](https://bitbucket.org/Axioms/riemann-sum-visualizer/src)

## Example
![Example](http://i.imgur.com/NvP7379.gif)

[(Direct Link (.gif))](http://i.imgur.com/NvP7379.gifv)

## Project Reflection
Determining the total area or volume under a curve or surface is an ability that gives valuable information about whatever the curve is describing, be it a simple, geometric description or complex, physical model. A common, relatively simple method of approximation is through use of _Riemann Sums_, wherein the region is divided into many small, basic shapes, which can then be summed together to yield an approximation of the total region.

![Graph](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Riemann_sum_%28middlebox%29.gif/240px-Riemann_sum_%28middlebox%29.gif)  

[(Direct Link (.gif))](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Riemann_sum_%28middlebox%29.gif/240px-Riemann_sum_%28middlebox%29.gif)  

The above diagram illustrates this process with a 2D curve, showing a progressive increase the number of divisions used. 

To obtain a more accurate solution, instead of only an approximation, a _definite integral_ can be utilized instead (For 3D functions: double integrals). The result of these can be seen as akin to making the number of divisions for the Riemann sum infinite, which would cause the step sizes of the dividing shapes to become infinitesimally small, as `dx` and `dy` in the integrals represent (infinitesimally small changes in x and y).



![Graph](http://i.imgur.com/uOL6KUi.png)  

[(Direct Link (.png))](http://i.imgur.com/uOL6KUi.png)  

In this demonstration, the function `z=cos(x^2 + y^2)` was used. Above is a complete graph of the shape.

## Additional Details
This program was created using the [SFML](http://www.sfml-dev.org/) graphics library. The 3D rendering engine is built off the [perspective projection](https://bitbucket.org/Axioms/perspective-projection/src) project created as a demonstration of a practical application of vectors for [Journal #1](https://bitbucket.org/Axioms/perspective-projection/src).