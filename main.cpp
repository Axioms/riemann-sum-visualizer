/**
 * Riemann sum visualizer
 */

#include <iostream>
#include <cmath>
#include <array>
#include <vector>
#include <string>
#include <SFML/Graphics.hpp>

const int SCREEN_WIDTH = 1400;
const int SCREEN_HEIGHT = 1400;
const int NUM_POINTS = 10000;
const int AXIS_LENGTH = 2000;
const int ZOOM = 100;
const float SPEED = 5;

struct Vector3 {
    float x;
    float y;
    float z;

    //Construct vector
    Vector3(float x0 = 0, float y0 = 0, float z0 = 0) :
            x(x0),
            y(y0),
            z(z0) {
    }

    //Magnitude of vector
    float magnitude() {
        return sqrt(x * x + y * y + z * z);
    }

    //Dot product with another vector
    float dot(Vector3 other) {
        return x * other.x + y * other.y + z * other.z;
    }

    //Cross product with another vector (<this> x <other>)
    Vector3 cross(const Vector3 other) const {
        return Vector3(y * other.z - z * other.y, -(x * other.z - z * other.x),
                x * other.y - y * other.x);
    }

    //Overload + operator
    friend Vector3 operator+(const Vector3 &v1, const Vector3 &v2);
};

Vector3 operator+(const Vector3 &v1, const Vector3 &v2) {
    return Vector3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

std::array<bool, sf::Keyboard::KeyCount> keyStates;

bool isKeyPressed(sf::Keyboard::Key key) {
    return keyStates[static_cast<int>(key)];
}

void setPoint(Vector3 &v, Vector3 value) {
    v.x = value.x;
    v.y = value.y;
    v.z = value.z;
}

struct Rect3D: public sf::Drawable {
    Vector3 v[24];
    sf::VertexArray vertices;
    Vector3 pos;
    Vector3 size;
    sf::Color color;

    Rect3D(Vector3 pos, Vector3 size, sf::Color color) :
            vertices(sf::PrimitiveType::Lines, 24),
            pos(pos),
            size(size),
            color(color) {
        set(pos, size);
    }

    void set(Vector3 p, Vector3 s) {
        pos = p;
        size = s;

        for (int i = 0; i < 2; i++) {
            v[0 + i * 8] = Vector3(pos.x, pos.y, pos.z + size.z * i);
            v[1 + i * 8] = Vector3(pos.x + size.x, pos.y, pos.z + size.z * i);
            v[2 + i * 8] = Vector3(pos.x, pos.y, pos.z + size.z * i);
            v[3 + i * 8] = Vector3(pos.x, pos.y + size.y, pos.z + size.z * i);
            v[4 + i * 8] = Vector3(pos.x + size.x, pos.y, pos.z + size.z * i);
            v[5 + i * 8] = Vector3(pos.x + size.x, pos.y + size.y,
                    pos.z + size.z * i);
            v[6 + i * 8] = Vector3(pos.x, pos.y + size.y, pos.z + size.z * i);
            v[7 + i * 8] = Vector3(pos.x + size.x, pos.y + size.y,
                    pos.z + size.z * i);
        }
        v[16] = Vector3(pos.x, pos.y, pos.z);
        v[17] = Vector3(pos.x, pos.y, pos.z + size.z);
        v[18] = Vector3(pos.x + size.x, pos.y, pos.z);
        v[19] = Vector3(pos.x + size.x, pos.y, pos.z + size.z);
        v[20] = Vector3(pos.x, pos.y + size.y, pos.z);
        v[21] = Vector3(pos.x, pos.y + size.y, pos.z + size.z);
        v[22] = Vector3(pos.x + size.x, pos.y + size.y, pos.z);
        v[23] = Vector3(pos.x + size.x, pos.y + size.y, pos.z + size.z);
    }

    void move(Vector3 delta) {
        set(Vector3(pos.x + delta.x, pos.y + delta.y, pos.z + delta.z), size);
    }

    void changeSize(Vector3 delta) {
        set(pos, Vector3(size.x + delta.x, size.y + delta.y, size.z + delta.z));
    }

    void update(Vector3 normal, Vector3 screenHorizontal, Vector3 camera,
            Vector3 camPos) {
        for (int i = 0; i < vertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (v[i].x - camPos.x)
                            + normal.y * (v[i].y - camPos.y)
                            + normal.z * (v[i].z - camPos.z));

            pos.x = camera.x + (v[i].x - camPos.x) * t;
            pos.y = camera.z + (v[i].z - camPos.z) * t;

            //Don't draw line if one of its vertices is offscreen
            bool skipNext = false;
            if (v[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;

                if (i % 2 == 0) {
                    vertices[i + 1] = sf::Vertex(pos);
                    skipNext = true;
                } else {
                    vertices[i - 1] = sf::Vertex(pos);
                }
            }

            sf::Color c = color;

            //Darken tops so depth can be seen
            if(i>=8 && i <=15) {
                c = sf::Color(color.r * 0.7, color.g * 0.7, color.b * 0.7);
            }

            vertices[i] = sf::Vertex(pos, c);
            if (skipNext) {
                i++;
            }
        }
    }

    void draw(sf::RenderTarget & target, sf::RenderStates states) const {
        target.draw(vertices, states);
    }
};

float eval(float x, float y) {
    x /= ZOOM;
    y /= ZOOM;

    return std::cos(x * x + y * y) * ZOOM;
}

int main() {
    /////////// Window and points setup ////////////

    //Set-up the window
    sf::VideoMode videoMode(SCREEN_WIDTH, SCREEN_HEIGHT);
    sf::RenderWindow renderWindow(videoMode, "Riemann Sum Visualizer");

    keyStates.fill(false);

    //Set-up points
    srand(time(NULL));
    Vector3 points[NUM_POINTS];
    points[0].x = 0;
    points[0].y = 0;
    points[0].z = 0;

    //Make axis
    Vector3 axis[6];
    axis[0] = Vector3(-AXIS_LENGTH / 2, 0, 0);
    axis[1] = Vector3(AXIS_LENGTH / 2, 0, 0);
    axis[2] = Vector3(0, -AXIS_LENGTH / 4, 0);
    axis[3] = Vector3(0, AXIS_LENGTH / 4, 0);
    axis[4] = Vector3(0, 0, -AXIS_LENGTH);
    axis[5] = Vector3(0, 0, AXIS_LENGTH);

    sf::VertexArray axisVertices(sf::PrimitiveType::Lines, 6);
    sf::VertexArray vertices(sf::PrimitiveType::Lines, NUM_POINTS);

    std::vector<Rect3D> rects;
//    Rect3D r(Vector3(100, 100, 100), Vector3(100, 200, 300), sf::Color::Red);

    int m = 5;
    int n = 5;
    Rect3D bounds(Vector3(0, 0, 0), Vector3(200, 200, 0), sf::Color::Red);
    float xDiv = bounds.size.x / m;
    float yDiv = bounds.size.y / n;
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            Rect3D r(
                    Vector3(bounds.pos.x + xDiv * i, bounds.pos.y + yDiv * j,
                            bounds.pos.z),
                    Vector3(xDiv, yDiv,
                            eval(bounds.pos.x + xDiv * i,
                                    bounds.pos.y + yDiv * j)),
                    sf::Color::Green);
            rects.push_back(r);
        }
    }

    Vector3 screenHorizontal(SCREEN_WIDTH, 0, 0);
    Vector3 screenVertical(0, 0, SCREEN_HEIGHT);
    Vector3 camera(SCREEN_WIDTH / 2, -1000, SCREEN_HEIGHT / 2);
    Vector3 camPos(0, -1000, 0);

    sf::Font font;
    font.loadFromFile("Roboto-Black.ttf");
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(45);
    text.setPosition(10, 0);

    /////////// Main program loop ////////////
    while (renderWindow.isOpen()) {

        /////////// Handle input events ////////////
        sf::Event event;
        while (renderWindow.pollEvent(event)) {
            switch (event.type) {
                case (sf::Event::Closed):
                    renderWindow.close();
                    break;
                case (sf::Event::KeyPressed):
                    keyStates[static_cast<int>(event.key.code)] = true;
                    if (event.key.code == sf::Keyboard::Escape) {
                        renderWindow.close();
                    }
                    break;
                case (sf::Event::KeyReleased):
                    keyStates[static_cast<int>(event.key.code)] = false;
                    break;
                default:
                    break;
            }
        }
        if (isKeyPressed(sf::Keyboard::Left)) {
            camPos.x -= SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Right)) {
            camPos.x += SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Up)) {
            camPos.z -= SPEED;
        }
        if (isKeyPressed(sf::Keyboard::Down)) {
            camPos.z += SPEED;
        }
        if (isKeyPressed(sf::Keyboard::W)) {
            camPos.y += SPEED / 2;
        }
        if (isKeyPressed(sf::Keyboard::S)) {
            camPos.y -= SPEED / 2;
        }

        if (isKeyPressed(sf::Keyboard::I)) {
            bounds.move(Vector3(0, SPEED / 2, 0));
        }
        if (isKeyPressed(sf::Keyboard::K)) {
            bounds.move(Vector3(0, -SPEED / 2, 0));
        }
        if (isKeyPressed(sf::Keyboard::J)) {
            bounds.move(Vector3(-SPEED / 2, 0, 0));
        }
        if (isKeyPressed(sf::Keyboard::L)) {
            bounds.move(Vector3(SPEED / 2, 0, 0));
        }
        if (isKeyPressed(sf::Keyboard::O)) {
            bounds.changeSize(Vector3(SPEED, SPEED, 0));
        }
        if (isKeyPressed(sf::Keyboard::U)) {
            bounds.changeSize(Vector3(-SPEED, -SPEED, 0));
        }

        if (isKeyPressed(sf::Keyboard::R)) {
            m++;
            n++;
        }
        if (isKeyPressed(sf::Keyboard::F)) {
            m--;
            n--;
        }

        //Update Riemann sum boxes
        rects.clear();
        float xDiv = bounds.size.x / m;
        float yDiv = bounds.size.y / n;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                Rect3D r(
                        Vector3(bounds.pos.x + xDiv * i,
                                bounds.pos.y + yDiv * j, bounds.pos.z),
                        Vector3(xDiv, yDiv,
                                eval(bounds.pos.x + xDiv * i,
                                        bounds.pos.y + yDiv * j)),
                        sf::Color::Green);
                rects.push_back(r);
            }
        }

        Vector3 normal = screenHorizontal.cross(screenVertical);
        //Update 2D projections of axis
        for (int i = 0; i < axisVertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (axis[i].x - camPos.x)
                            + normal.y * (axis[i].y - camPos.y)
                            + normal.z * (axis[i].z - camPos.z));

            pos.x = camera.x + (axis[i].x - camPos.x) * t;
            pos.y = camera.z + (axis[i].z - camPos.z) * t;

            //Don't draw line if one of its vertices is offscreen
            bool skipNext = false;
            if (axis[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;

                if (i % 2 == 0) {
                    axisVertices[i + 1] = sf::Vertex(pos);
                    skipNext = true;
                } else {
                    axisVertices[i - 1] = sf::Vertex(pos);
                }
            }
            axisVertices[i] = sf::Vertex(pos, sf::Color::Magenta);
            if (skipNext) {
                i++;
            }
        }

        //Update 2D projections of points
        for (int i = 0; i < vertices.getVertexCount(); i++) {
            sf::Vector2f pos;

            float t = (normal.z * screenHorizontal.z - normal.z * camera.z
                    + normal.y * screenHorizontal.y - normal.y * camera.y
                    + normal.x * screenHorizontal.x - normal.x * camera.x)
                    / (normal.x * (points[i].x - camPos.x)
                            + normal.y * (points[i].y - camPos.y)
                            + normal.z * (points[i].z - camPos.z));

            pos.x = camera.x + (points[i].x - camPos.x) * t;
            pos.y = camera.z + (points[i].z - camPos.z) * t;

            //Don't draw line if one of its vertices is offscreen
            bool skipNext = false;
            if (points[i].y < camPos.y) {
                pos.x = 0;
                pos.y = 0;

                if (i % 2 == 0) {
                    vertices[i + 1] = sf::Vertex(pos);
                    skipNext = true;
                } else {
                    vertices[i - 1] = sf::Vertex(pos);
                }
            }
            vertices[i] = sf::Vertex(pos, sf::Color(180, 180, 180));
            if (skipNext) {
                i++;
            }
        }

        float volume = 0;
        for(Rect3D &r : rects) {
            volume += r.size.x * r.size.y * r.size.z;
        }

        std::string s = "cos(x^2 + y^2)";
        s.append("\nm: ").append(std::to_string(m));
        s.append("\nn: ").append(std::to_string(n));
        s.append("\nVolume: ").append(std::to_string(-volume));
        text.setString(s);

        /////////// Update window display ////////////
        renderWindow.clear(sf::Color::Black);

        renderWindow.draw(vertices);
        renderWindow.draw(axisVertices);

        for (Rect3D &r : rects) {
            r.update(normal, screenHorizontal, camera, camPos);
            renderWindow.draw(r);
        }
        bounds.update(normal, screenHorizontal, camera, camPos);
        renderWindow.draw(bounds);

        renderWindow.draw(text);

        renderWindow.display();
    }
}

